import React, { useState, Fragment } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';
import { getPlatforms } from '@ionic/react';
import QrReader from 'react-qr-reader'
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

const platforms = ["mobileweb", "desktop"];
// Use array.include to check

const Tab1: React.FC = () => {
    const [state, setState] = useState({
        result: 'No result'
    })

    const handleScan = (data: any) => {
        if (data) {
            setState({
                result: data
            })
        }
    }
    const handleError = (err: any) => {
        console.error(err)
    }
    const openScanner = async () => {
        const data = await BarcodeScanner.scan();
        console.log(`Barcode data: ${data.text}`);
    };

    const isNotMobileApp = () => getPlatforms().some(platform => platforms.includes(platform));

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Tab 1</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                {isNotMobileApp() ? (
                    <Fragment>
                        <QrReader
                            delay={300}
                            onError={handleError}
                            onScan={handleScan}
                            style={{ width: '100%' }}
                        />
                        <p>{state.result}</p>
                    </Fragment>
                ) : (
                    <IonButton onClick={openScanner}>Scan barcode</IonButton>
                )}

            </IonContent>
        </IonPage>
    );
};

export default Tab1;
